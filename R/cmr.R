
#' Convert data frames to STACMR compatible formats
#'
#' @param d data frame to convert
#' @param idvar (bare) variable indicating participant (default: workerid)
#' @param state_factor character vector of column names that will be used for state-factor (e.g., axes of state trace plot)
#' @param withinvar bare column name specifying the conditions manipulated within-subject
#'
#' @return data frames ready for STACMR
#'
#' @details Assumes that there are no between-subject variables
#'
#' the different functions are for the different kinds of STA, outlined in package
#'
#' @name as_cmr
#' @references
#'   \url{https://github.com/michaelkalish/STA/tree/master}
NULL


#' @rdname as_cmr
#'
#' @import dplyr
#' @import dplyr
#' @import tidyr
#' @import rlang
#' @import tidyselect
#' @import stringr
#' @export
as_cmr_continuous <- function(d,
                              idvar = workerid,
                              state_factor = c("afc_result", "name_result"),
                              withinvar = study_code) {
  idvar_enq <- rlang::enquo(idvar)
  withinvar_enq <- rlang::enquo(withinvar)

  out <- d %>%
    dplyr::mutate(betweenvars = 1) %>%
    tidyr::gather(question, result, tidyselect::one_of(state_factor)) %>%
    dplyr::mutate(question = if_else(stringr::str_detect(question, state_factor[1]), 1, 2)) %>%
    dplyr::group_by(!!idvar_enq, betweenvars, question, !!withinvar_enq) %>%
    dplyr::summarise(result = mean(result)) %>%
    tidyr::spread(!!withinvar_enq, result) %>%
    as.data.frame()

  return(out)
}

#' @rdname as_cmr
#'
#' @import dplyr
#' @import dplyr
#' @import tidyr
#' @import rlang
#' @import tidyselect
#' @import stringr
#' @export
as_cmr_bn <- function(d,
                      idvar = workerid,
                      state_factor = c("afc_result", "name_result"),
                      withinvar = study_code) {
  idvar_enq <- rlang::enquo(idvar)
  withinvar_enq <- rlang::enquo(withinvar)

  out <- d %>%
    tidyr::gather(question, result, tidyselect::one_of(state_factor)) %>%
    dplyr::mutate(question = if_else(stringr::str_detect(question, state_factor[1]), 1, 2)) %>%
    dplyr::group_by(!!idvar_enq, !!withinvar_enq, question) %>%
    dplyr::summarise(
      successes = sum(result),
      failures = sum(!result)
    ) %>%
    as.data.frame()

  return(out)
}
