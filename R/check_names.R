
.anonymize <- function(d, KEY = "salt"){
  out <- d %>%
    dplyr::mutate(uniqueid = stringr::str_replace(uniqueid,
                                                  workerid,
                                                  openssl::sha256(workerid, key = KEY)),
                  datastring = dplyr::case_when(is.na(datastring) ~ datastring,
                                                TRUE ~ stringr::str_replace_all(datastring,
                                                                                workerid,
                                                                                openssl::sha256(workerid, key = KEY))),
                  workerid = openssl::sha256(workerid, key = KEY)
    )

  return(out)
}


.check_names <- function(d, responses, targets, cutoff = 1){
  responses_quo <- rlang::enquo(responses)

  targets %<>% dplyr::mutate_all(stringr::str_remove_all, "[[:space:]]|[[:punct:]]")

  name_result <- d %>%
    dplyr::mutate(targets = purrr::map(object, ~targets[.x,] %>%
                                         purrr::as_vector() %>%
                                         na.omit()),
                  responses_given = stringr::str_remove_all(!!responses_quo, "[[:space:]]|[[:punct:]]")) %>%
    dplyr::mutate(dl = purrr::map2(targets, responses_given,
                                   ~stringdist::stringdist(.x, .y, method="dl")),
                  contained = purrr::map2_lgl(targets,
                                              responses_given,
                                              ~any(str_detect(.x, .y), na.rm = TRUE ) ) ) %>%
    dplyr::mutate(minDist = purrr::map_dbl(dl, ~min(.x, na.rm=TRUE))) %>%
    dplyr::mutate(result = dplyr::if_else(minDist < cutoff | contained , TRUE, FALSE))

  d %<>% dplyr::mutate(name_result = name_result$result)

  return(d)
}

.check_correct_side <- function(stimuli){
  ap <- stringr::str_extract(stimuli, "ap-[[:digit:]]") %>%
    stringr::str_extract("[[:digit:]]")

  if(all(ap[1:2] == c("1","2"))){
    side <- "left"
  }else{
    side <- "right"
  }
  return(side)
}

