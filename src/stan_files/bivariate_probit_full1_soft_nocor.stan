functions{
#include /functions/binormal.stan
}
data {
  int<lower=0> n; // number of observations
  int<lower=1> n_condition; // number of conditions (4)
  int<lower=1,upper=n_condition> condition[n];
  int<lower=1> n_subject; // The number of subjects
  int<lower=1,upper=n_subject> subject[n]; // Index indicating the subject for a current trial
  int<lower=1> n_item; // The number of subjects
  int<lower=1,upper=n_item> item[n]; // Index indicating the subject for a current trial
  matrix<lower = 0, upper=1>[n, 2] y;
  vector[8] priors;
  matrix<lower=0, upper=1>[n,n_condition] X[2];
  int<lower=0, upper = 1>prior_only;  // should the likelihood be ignored?
}
transformed data{
  int D = 2;
  vector[n_condition-1] alpha_zeta = rep_vector(priors[3], n_condition-1);
  cholesky_factor_corr[D] L = diag_matrix([1,1]');
}
parameters{
  matrix[n_condition-1, n_subject] zeta_raw[D]; // -1 for intercept
  matrix<lower=0>[D, n_subject] beta_raw;
  matrix<lower=0>[D, 2] random_scales;
  matrix[D, n_subject] beta_intercept_z; // Subject-level coefficients for the bivariate normal means
  vector[D] beta_intercept; // Subject-level coefficients for the bivariate normal means
  matrix[D, n_item] item_mu_raw; // Subject-level coefficients for the bivariate normal means
}
transformed parameters {
  matrix[n_item, D] item_mu = (diag_pre_multiply(random_scales[,1], L ) * item_mu_raw)';
  row_vector[n] log_lik;

	{
	  matrix[D,D] subject_Sigma_L = diag_pre_multiply(random_scales[,2], L);
    matrix[n_condition, D] condition_mu_ordered[n_subject]; // condition effects on mean of latent
    matrix[n,D] mu;
    matrix[D, n_subject] subject_intercept;

    subject_intercept = (subject_Sigma_L * beta_intercept_z) + rep_matrix(beta_intercept, n_subject);

    for(s in 1:n_subject){
      condition_mu_ordered[s] = append_row(subject_intercept[,s]',
        append_col(beta_raw[1,s] * cumulative_sum( softmax(zeta_raw[1,,s]) ) ,
        beta_raw[2,s] * cumulative_sum( softmax(zeta_raw[2,,s]) ) ));
    }
    for(i in 1:n){
      mu[i] = [X[1,i] * condition_mu_ordered[subject[i],,1], X[2,i] * condition_mu_ordered[subject[i],,2]];
    }
    log_lik = biprobit_lpdf_vector(y, mu+item_mu[item], rep_vector(0, n) );
	}
}
model {

  // priors
	for(d in 1:D){
		to_vector(zeta_raw[d]) ~ normal(0, priors[3]);
	}
  to_vector(random_scales) ~ gamma(priors[4], priors[5]);
  to_vector(item_mu_raw) ~ std_normal();

  to_vector(beta_raw) ~ normal(0, priors[2]);
  to_vector(beta_intercept_z) ~ std_normal();
  beta_intercept ~ normal(0, priors[1]);

  if (!prior_only) {
    target += sum(log_lik);
  }
}
