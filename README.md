
<!-- README.md is generated from README.Rmd. Please edit that file -->

# sadilcowellhuber2019

This package contains analysis scripts for Sadil, Cowell, & Huber (2019)
“A hierarchical Bayesian state trace analysis for assessing monotonicity
while factoring out subject, item, and trial level dependencies”
[OSF](https://osf.io/ka7jv/).

## Installation

Note that much of the code in this package invovles either direct copies
of or rewrites of the code provided by two other packages for conducting
State-Trace Analyses.

  - Dunn, J. C. & Kalish, M. L. (2018). State-Trace Analysis. Springer.
    – [stacmr](https://github.com/michaelkalish/STA)

  - Davis-Stober, C. P., Morey, R. D., Gretton, M., & Heathcote, A.
    (2016). Bayes factors for state-trace analysis. Journal of
    Mathematical Psychology, 72, 116–129.
    <https://doi.org/10.1016/j.jmp.2015.08.004>

I have tried to be clear about when code comes from what was produced by
those authors.

Package rPorta is listed as a dependency for the use of analyses
presented by Davis-Stober, C. P., Morey, R. D., Gretton, M., &
Heathcote, A. (2016). Bayes factors for state-trace analysis. Journal of
Mathematical Psychology, 72, 116–129.
<https://doi.org/10.1016/j.jmp.2015.08.004> (see files abf.R,
example\_ABF\_analysis, and statetracelaplace.R). rPorta is no longer on
cran but can be installed
with

``` r
install.packages("https://cran.r-project.org/src/contrib/Archive/rPorta/rPorta_0.1-9.tar.gz", repo=NULL, type="source")
```

The R code in this package can then be installed with

``` r
# install.packages("remotes")
remotes::install_gitlab("psadil/sadilcowellhuber2019")
```

# Stan Files

The two stan files are *bivariate\_probit\_full1\_soft\_4c.stan* and
*bivariate\_probit\_full1\_soft\_nocor.stan*, which implements the main
model described in the MS and the version that assumes independences,
respectively.
